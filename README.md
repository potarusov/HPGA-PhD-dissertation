# Hybrid Parallel Genetic Approach to Solve 1D Bin Packing Problem

## PhD Dissertation Extended Abstract (Year of submission: 2008)

This dissertation considers the one-dimensional bin packing problem (BPP) that aims at combining items with different weights into bins of the same capacity so as to minimize the total number of bins. BPP is a model for several optimization problems in computer science and industrial engineering, such as table formatting, pre-paging, file allocation, etc. There are many industrial problems that seem to be different, but have a very similar structure, such as capital budgeting, processor scheduling and VLSI design. BPP is a combinatorial NP-hard problem.

Even today, despite the fact that BPP is a well studied problem, there are a lot of problem instances/sets that existing solution methods do not solve optimally. The best results previously reported in the literature were not all obtained by a single heuristic or meta-heuristic.

Genetic algorithms provide an approach to learning that is based on simulated evolution. They have been successfully applied to a variety of learning tasks as well as to optimization problems. To the author’s knowledge, the only application of genetic algorithms to solve BPP was the Hybrid Grouping Genetic Algorithm (HGGA) [1]. The HGGA outperformed the Marthello and Toth Reduction Procedure [2], which constitutes one of the best Operations Research techniques for solving BPP to date.

The main contributions of my dissertation research are:

1. A novel Hybrid Parallel Genetic Approach (HPGA) [3] to solve BPP. It uses three genetic operators to make a population of solutions, i.e. chromosomes, evolve over time. The approach is parallel, because there are two hybrid genetic algorithms (HGA) working simultaneously. Thus, there are two sub-populations evolving separately over time. At each iteration (generation), a number of solutions from each sub-population migrate to the other sub-population.

2. An adaptation of the Hugo de Vries’ model of evolution for the context of solving BPP. His model is based on step-wise (abrupt, intermittent) changes, like catastrophes, for example. The HGA, which adapts this model, aims at obtaining “strong” solutions with an objective function value greater than *V*. Every *N* iterations solutions with an objective function value less than *V* are replaced by new ones that are created at random. Thus the “strong” solutions have better chances at going to the next iteration of the HGA and reproducing. After several iterations of the HGA, solutions of this type would dominate a population. Solutions with an objective function value less than *V* die in these conditions.

3. An adaptation of the Jean-Baptiste Lamarck’s model of evolution for the context of solving the BPP. The main idea of his model is: child solutions inherit all useful features acquired by parents during their life process. The HGA, which adapts this model, aims at obtaining solutions that contain full bins (at least one bin). Thus, these solutions have better chances at going to the next iteration of the HGA and reproducing. After several iterations of the HGA, solutions of this type would dominate a population.

4. New problem-oriented genetic operators (crossover, mutation, and transposition) capable of obtaining valid (feasible) BPP solutions.

5. Two efficient local search algorithms (as a part of genetic operators). The application of genetic operators often results in unfeasible solutions. In BPP, for example, solutions are often incomplete (not all the elements/items are present in bins) or the same elements appear in multiple bins. The proposed approach is hybrid, because the genetic operators applied to chromosomes use two local search procedures to "fix" the solutions obtained: either by inserting missing items back into solutions, or removing “duplicates”.

The developed approach was compared with the state-of-the-art algorithms, including:

1. Exact algorithms: Martello-Toth Reduction Procedure, BISON, Branch-and-Bound [2,4,5];

2. Approximative algorithms: First-Fit Decreasing, Best-Fit Decreasing [2, 7-9];

3. Metaheuristics: HGGA [1], Ant Colony Optimization [10], Simulated Annealing [11].

The HPGA outperformed all of these algorithms for some of the most difficult problem instances. Computational experiments showed that the HPGA obtains near-optimal and optimal solutions for 1370 problem instances found in the literature. In the case of near-optimal solutions, the absolute deviation from reference solution is at most one bin. Worst-case time complexity of the HPGA is O(*n<sup>2</sup>L<sup>2</sup>*), where *n* is the population size and *L* is the length of a candidate solution/chromosome.

## References

[1] Falkenauer, E. (1996). A Hybrid Grouping Genetic Algorithm for Bin Packing. Journal of Heuristics, 2, 5–30.

[2] Martello S. and P. Toth. Knapsacks Problems: Algorithms and Computer Implementations (1990). Chichester/England: John Wiley and sons Ltd.

[3] Potarusov, R., Allaoui H., Goncalves G. and Kureychik, V., “Hybrid Genetic Approach for 1-D Bin Packing Problem. International Journal of Services Operations and Informatics,” Volume 6, Number 1-2/2011, 71-85.

[4] Scholl A., Klein R. and C. Jürgens (1997). BISON: a Fast Hybrid Procedure for Exactly Solving the One-Dimensional Bin Packing Problem, Comput. Oper. Res., 24, 627–645.

[5] Valerio de Carvalho, J. M. (1999). Exact Solution of Bin-Packing Problems Using Column Generation and Branch-and-Bound. Annals of Operations Research, 86, 629-659.

[6] Emelianov V., Kureychik V. M. and V. V. Kureychik. (2003). Theory and Practice of Evolutionary Modelling. “Fizmatlit”, Мoscow, 432 p.

[7] Fleszar, K. and K. Hindi. (2002). New Heuristics for One-Dimensional Bin Packing. Computers and Operations Research 29(7), 821–839.

[8] Gupta, J. N. D, Ho J. C (1999). A New Heuristic Algorithm for the One-Dimensional Bin- Packing Problem. Production Planning & Control, 10(6), 598-603.

[9] Bai R. (2005). An Investigation of Novel Approaches for Optimizing Retail Shelf Space Allocation. Ph. D thesis. School of Computer Science & Information Technology. The University of Nottingham, Nottingham, UK.

[10] Levine J. and F. Ducatelle (2003). Ant Colony Optimization and Local Search for Bin Packing and Cutting Stock Problems. Centre for Intelligent Systems and their Applications, School of Informatics, University of Edinburgh.

[11] Rao, R. L. and Iyengar, S. S. Bin-Packing by Simulated Annealing, Computers & Mathematics with Applications. 27, 1994, 71-82.
