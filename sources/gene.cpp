#include <iostream>
#include "gene.h"

Gene::Gene(void){
	sum_of_items = 0;
	to_be_removed = false;
}

Gene::~Gene(){
    items.clear();
}

int Gene::GetSumOfItems(){
	return sum_of_items;
}

void Gene::AppendItem(Item &item){
    items.push_back(item);
	sum_of_items += item.GetValue();

}

void Gene::PrintGene(){
    for( int i = 0; i < items.size(); i++ ){
        cout << "Item: " << i << endl;
        items[i].PrintItem();
    }
}


