#include <iostream>
#include "chromosome.h"

Chromosome::Chromosome(void){
    m_bin_capacity = 0.0;
    m_fitness = 0.0;
}

Chromosome::Chromosome(int _capacity){
    m_bin_capacity = _capacity;
    m_fitness = 0.0;
}

Chromosome::Chromosome(const Chromosome &obj){
	m_bin_capacity = obj.m_bin_capacity;
	m_fitness = obj.m_fitness;

	genes.resize(obj.genes.size());
	std::copy(obj.genes.begin(), obj.genes.end(), genes.begin());
}

Chromosome::~Chromosome(){
    genes.clear();
}

float Chromosome::ComputeFitness(){
    float sum1, sum2 = 0.0;
    for(int i = 0; i < genes.size(); i++){
        sum1 = 0.0;
        for(int j = 0; j < genes[i].items.size(); j++)
            sum1 += genes[i].items[j].GetValue();

        sum2 += sum1*sum1 / (m_bin_capacity*m_bin_capacity);
    }
    m_fitness = sum2/genes.size();
    return sum2/genes.size();
}

float Chromosome::ComputeFitness(int gene){
    float sum1, sum2 = 0.0;
    sum1 = 0.0;
    for(int j = 0; j < genes[gene].items.size(); j++)
        sum1 += genes[gene].items[j].GetValue();

    sum2 += sum1*sum1 / (m_bin_capacity*m_bin_capacity);

    return sum2;
}

float Chromosome::ComputeFitness(int start_gene, int end_gene){
    float sum1, sum2 = 0.0;
    for(int i = start_gene; i <= end_gene; i++){
        sum1 = 0.0;
        for(int j = 0; j < genes[i].items.size(); j++)
            sum1 += genes[i].items[j].GetValue();

        sum2 += sum1*sum1 / (m_bin_capacity*m_bin_capacity);
    }

    return sum2/(end_gene - start_gene);
}

// Appends items to a specified gene
void Chromosome::InsertItemToGene(int gene_index_in_chromosome, Item &item){
    genes[gene_index_in_chromosome].AppendItem(item);
}

// Inserts genes at a specified position: position - iterator before which the content will be inserted
void Chromosome::InsertGeneToChromosome(int position, Gene &gene){
    genes.insert(genes.begin() + position, gene);
}

// Inserts an array of genes at a specified position
void Chromosome::InsertArrayToChromosome(int position, std::vector<Gene> &arr){
    genes.insert(genes.begin() + position, arr.begin(), arr.end());
}

void Chromosome::AppendGene(Gene &gene){
    genes.push_back(gene);
}

void Chromosome::DeleteGene(int gene){
	genes.erase(genes.begin() + gene);
}

void Chromosome::PrintChromosome(){
    for( int i = 0; i < genes.size(); i++ ){
        cout << "Gene: " << i << endl;
        genes[i].PrintGene();
    }
}


