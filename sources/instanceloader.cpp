#include "instanceloader.h"
#include <iostream>
#include <fstream>
#include <sstream>

InstanceLoader::InstanceLoader(void){
    m_strDataFileName = nullptr;
    m_nb_items = 0;
    m_bin_capacity = 0;
}

InstanceLoader::InstanceLoader(const char *strDataFileName){
    m_strDataFileName = strDataFileName;
    m_nb_items = 0;
    m_bin_capacity = 0;
}

InstanceLoader::~InstanceLoader(){
    items.clear();
}

int InstanceLoader::LoadData(){
    if ( m_strDataFileName ){
        string line;
        string::size_type n;
        ifstream is;
        is.open(m_strDataFileName);

        try{
            if ( is ){
                getline(is,line);
                m_nb_items = stoi(line, &n);
                getline(is,line);
                m_bin_capacity = stoi(line, &n);

                for( int i = 0; i < m_nb_items; i++ ){
                    getline(is,line);
                    items.push_back(stoi(line, &n));
                }
            }
        }
        catch( int e ){
            is.close();
        }
        return 0;
    }
    else
        return 1;
}

void InstanceLoader::PrintInstance(){
    cout << "Instance Name: " << m_strDataFileName << endl;
    cout << "Number of Items: " << m_nb_items << endl;
    cout << "Bin Capacity: " << m_bin_capacity << endl;

    for(int i = 0; i < items.size(); i++)
        cout << "Item: " << i << " Value: " << items[i] << endl;
}







