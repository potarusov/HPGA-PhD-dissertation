#include <iostream>
#include <algorithm>
#include "hga.h"

BaseHGA::BaseHGA(void){
    m_bin_capacity = 0;
    m_crossover_probability = 0;
    m_mutation_probability = 0;
}

BaseHGA::BaseHGA(int bin_capacity, float crossover_probability, float mutation_probability){
    m_bin_capacity = bin_capacity;
    m_crossover_probability = crossover_probability;
    m_mutation_probability = mutation_probability;
}

BaseHGA::~BaseHGA(){
}

// Heuristics
// First Fit
void BaseHGA::FFHeuristic(vector<Item> &items_to_insert, Chromosome &chromosome){
	bool is_inserted = false;
	for (int i = 0; i < items_to_insert.size(); i++){
		is_inserted = false;
		for (int j = 0; j < chromosome.genes.size(); j++)
			if (chromosome.genes[j].GetSumOfItems() + items_to_insert[i].GetValue() <= m_bin_capacity){
				chromosome.genes[j].AppendItem(items_to_insert[i]);
				is_inserted = true;
				break;
			}
		if (!is_inserted){
			Gene gene;
			gene.AppendItem(items_to_insert[i]);
			chromosome.genes.push_back(gene);
		}
	}
}

// Best Fit Decreasing
void BaseHGA::BFDHeuristic(vector<Item> &items_to_insert, Chromosome &chromosome){
    std::sort(items_to_insert.begin(), items_to_insert.end());

    vector<int> space_left;
	for (int i = 0; i < items_to_insert.size(); i++){
		for (int j = 0; j < chromosome.genes.size(); j++){
			if (chromosome.genes[j].GetSumOfItems() + items_to_insert[i].GetValue() <= m_bin_capacity)
				space_left.push_back(m_bin_capacity - chromosome.genes[j].GetSumOfItems() - items_to_insert[i].GetValue());
			else
				space_left.push_back(m_bin_capacity + 1);
		}

		std::vector<int>::iterator result = std::min_element(std::begin(space_left), std::end(space_left));
        int min_element_at = std::distance(std::begin(space_left), result);

		if (space_left[min_element_at] != m_bin_capacity + 1)
			chromosome.genes[min_element_at].AppendItem(items_to_insert[i]);
		else{
			Gene gene;
			gene.AppendItem(items_to_insert[i]);
			chromosome.genes.push_back(gene);
		}
		space_left.clear();
	}
}

// Local search procedure proposed by E. Falkenauer
void BaseHGA::EFLocalSearchProcedure(vector<Item> &items_to_insert, Chromosome &chromosome){
    bool replacement = true;

    while(replacement && !items_to_insert.empty()){
        replacement = false;
        for(int i = 0; i < chromosome.genes.size(); i++){
            if(chromosome.genes[i].GetSumOfItems() == m_bin_capacity)
                continue;
            for(int j = 0; j < chromosome.genes[i].items.size(); j++){
                if((chromosome.genes[i].GetSumOfItems() - chromosome.genes[i].items[j].GetValue() + items_to_insert[0].GetValue() <= m_bin_capacity)
                    && (items_to_insert[0].GetValue() > chromosome.genes[i].items[j].GetValue())){
                    items_to_insert.push_back(chromosome.genes[i].items[j]);
                    chromosome.genes[i].items[j].SetId(items_to_insert[0].GetId());
                    chromosome.genes[i].items[j].SetValue(items_to_insert[0].GetValue());
                    items_to_insert.erase(items_to_insert.begin());
                    replacement = true;
                }
            }
        }
    }
}

// Roulette Wheel Selection Operator
int BaseHGA::RouletteWheelSelectionOpr(Population &population){
    // Calculate the sum of fitnesses of the population
    float sum_of_fitnesses = 0.0;
    for(int i = 0; i < population.GetSize(); i++)
        sum_of_fitnesses += population.chromosomes[i].ComputeFitness();

    // Calculate the selection probability for all individuals of the population
    float selection_probability = 0;
    vector<float> selection_probabilities;
    for(int i = 0; i < population.GetSize(); i++){
        selection_probability = population.chromosomes[i].ComputeFitness()/sum_of_fitnesses;
        selection_probabilities.push_back(selection_probability);
    }

    // Generate a random number between 0 and 1
    float roulette_wheel_sector = (rand()%100 + 1)/100.0;

    // Starting from the top of the population, keep adding the probabilities to the partial sum
    // "sum_of_selection_probabilities", till "roulette_wheel_sector <= sum_of_selection_probabilities"
    float sum_of_selection_probabilities = 0.0;
    for(int i = 0; i < selection_probabilities.size(); i++){
        sum_of_selection_probabilities += selection_probabilities[i];

        if(roulette_wheel_sector <= sum_of_selection_probabilities)
            return i;
    }
    selection_probabilities.clear();
    return -1;
}

// Tournament Selection Operator
int BaseHGA::TournamentSelectionOpr(Population &population, int tournament_size){
    // We select "tournament_size" individuals from the population at random
    int index = 0;
    vector<int> selected_chromosomes;
    for(int i = 0; i < tournament_size; i++){
        index = rand()%population.GetSize();
        selected_chromosomes.push_back(index);
    }

    // Then select the best out of these to become a parent
    float max = 0.0;
    float fitness = 0.0;
    max = population.chromosomes[selected_chromosomes[0]].ComputeFitness();
    for( int j = 1; j < tournament_size; j++){
        fitness = population.chromosomes[selected_chromosomes[j]].ComputeFitness();
        if(fitness > max){
            max = fitness;
            index = j;
        }
    }
    selected_chromosomes.clear();
    return index;
}

bool IsItemToBeRemoved(const Item& item){
	if (item.GetId() == -1)
		return true;
	return false;
}

bool IsGeneEmpty(const Gene& gene){
	if (gene.items.empty())
		return true;

	return false;
}

bool IsGeneToBeRemoved(const Gene& gene){
	if (gene.to_be_removed)
		return true;

	return false;
}

// One-Point Crossover Operator
// First child: items from the first parent that are located after
// the cross-point are replaced by the corresponding items from the second parent
// Second child: items from the second parent that are located after
// the cross-point are replaced by the corresponding items from the first parent
void BaseHGA::OnePointCrossover(const Chromosome &first_parent, const Chromosome &second_parent, Chromosome &child){
    // 1. Generate two cross points:
    // For the first parent
    int cp_1st_parent = (rand() % (first_parent.genes.size() - 2));
    //int cp_1st_parent = 3;
	
    // For the second parent
    int cp_2nd_parent = (rand() % (second_parent.genes.size() - 2));
    //int cp_2nd_parent = 3;

    // 2. List the items to be deleted
	vector<Item> deleted_items;
	for (int i = cp_1st_parent; i < first_parent.genes.size(); i++){
		for (int j = 0; j < first_parent.genes[i].items.size(); j++)
			deleted_items.push_back(first_parent.genes[i].items[j]);
	}

    // 3. List the items to be inserted
	vector<Item> inserted_items;
	for (int i = cp_2nd_parent; i < second_parent.genes.size(); i++){
		for (int j = 0; j < second_parent.genes[i].items.size(); j++)
			inserted_items.push_back(second_parent.genes[i].items[j]);
	}

    // 4. If two lists cross, label the corresponding items as "to be deleted"
	for (int i = 0; i < deleted_items.size(); i++){
		for (int j = 0; j < inserted_items.size(); j++){
			if (deleted_items[i].GetId() == inserted_items[j].GetId()){
				deleted_items[i].SetId(-1);
				inserted_items[j].SetId(-1);
			}
		}
	}

    // 5. Delete the genes that are located after the cross point of the first parent
	child.genes.erase(child.genes.begin() + cp_1st_parent, child.genes.end());

    // 6. Append the genes the are located after the cross point of the second parent to the first parent
	child.genes.insert(child.genes.end(), second_parent.genes.begin() + cp_2nd_parent, second_parent.genes.end());

    // 7. Delete items labeled as "to be deleted" from two previously created lists
    deleted_items.erase(std::remove_if(deleted_items.begin(), deleted_items.end(), IsItemToBeRemoved), deleted_items.end());
	inserted_items.erase(std::remove_if(inserted_items.begin(), inserted_items.end(), IsItemToBeRemoved), inserted_items.end());

	// At this point, the list "deleted_items" contains items, which were "definitely" deleted from the child solution, so they are
	// to be re-inserted into it
	// The list "inserted_items" contains items, which have "duplicate" the items from the left part of the child solution

    // 8. Search for duplicates in the left part of the child solution
	for (int i = 0; i < inserted_items.size(); i++){
		for (int j = 0; j < cp_1st_parent; j++){
			for (int k = 0; k < child.genes[j].items.size(); k++){
				if (inserted_items[i].GetId() == child.genes[j].items[k].GetId()){
					child.genes[j].items[k].SetId(-1);
					child.genes[j].to_be_removed = true;
				}
			}
		}
	}

	// 9. Remove duplicates from the left part of the child solution
	for (int i = 0; i < cp_1st_parent; i++)
		child.genes[i].items.erase(std::remove_if(child.genes[i].items.begin(), child.genes[i].items.end(), IsItemToBeRemoved), child.genes[i].items.end());

	// 10. Remove empty bins
	child.genes.erase(std::remove_if(child.genes.begin(), child.genes.end(), IsGeneEmpty), child.genes.end());

	// 11. Append items from "to be removed" genes to the list of deleted items
	for (int i = 0; i < child.genes.size(); i++){
		if (child.genes[i].to_be_removed)
			for (int j = 0; child.genes[i].items.size(); j++)
				deleted_items.push_back(child.genes[i].items[j]);
	}

	// 12. Remove bins labeled "to be removed"
	child.genes.erase(std::remove_if(child.genes.begin(), child.genes.end(), IsGeneToBeRemoved), child.genes.end());

    // 13. Apply Falkenauer's local search procedure to optimize the first child solution
	EFLocalSearchProcedure(deleted_items, child);

    // 14. If there are items on "deleted_items" list, apply FF heuristic to insert them into the first child
    if (!deleted_items.empty())
		FFHeuristic(deleted_items, child);

	deleted_items.clear();
	inserted_items.clear();
}

void BaseHGA::OnePointCrossoverOpr(const Chromosome &first_parent, const Chromosome &second_parent, Chromosome &first_child, Chromosome &second_child){
	OnePointCrossover(first_parent, second_parent, first_child);
	OnePointCrossover(second_parent, first_parent, second_child);
}


// Two-Point Crossover Operator
void BaseHGA::TwoPointCrossover(const Chromosome &first_parent, const Chromosome &second_parent, Chromosome &child){
	int fcp_1st_parent = 0, scp_1st_parent = 0, fcp_2nd_parent = 0, scp_2nd_parent = 0;
	
	// 1. Generate two cross points:
	// for the first parent
	do{
        fcp_1st_parent = (rand() % (first_parent.genes.size() - 2));
        scp_1st_parent = (rand() % (first_parent.genes.size() - 2));
        //fcp_1st_parent = 2;
        //scp_1st_parent = 4;

	} while (fcp_1st_parent >= scp_1st_parent);

	// For the second parent
	do{
        fcp_2nd_parent = (rand() % (second_parent.genes.size() - 2));
        scp_2nd_parent = (rand() % (second_parent.genes.size() - 2));
        //fcp_2nd_parent = 2;
        //scp_2nd_parent = 4;
	} while (fcp_2nd_parent >= scp_2nd_parent);

	// 2. List the items to be deleted
	vector<Item> deleted_items;
	for (int i = fcp_1st_parent; i < scp_1st_parent; i++){
		for (int j = 0; j < first_parent.genes[i].items.size(); j++)
			deleted_items.push_back(first_parent.genes[i].items[j]);
	}
	
	// 3. List the items to be inserted
	vector<Item> inserted_items;
	for (int i = fcp_2nd_parent; i < scp_2nd_parent; i++){
		for (int j = 0; j < second_parent.genes[i].items.size(); j++)
			inserted_items.push_back(second_parent.genes[i].items[j]);
	}

	// 4. If two lists cross, label the corresponding items as "to be deleted"
	for (int i = 0; i < deleted_items.size(); i++){
		for (int j = 0; j < inserted_items.size(); j++){
			if (deleted_items[i].GetId() == inserted_items[j].GetId()){
				deleted_items[i].SetId(-1);
				inserted_items[j].SetId(-1);
			}
		}
	}

	// 5. Delete the genes that are located between the cross points of the first parent
	child.genes.erase(child.genes.begin() + fcp_1st_parent, child.genes.begin() + scp_1st_parent);
	
	// 6. Insert the genes the are located between the cross-points of the second parent into the first parent (child) after the first cross-point
	child.genes.insert(child.genes.begin() + fcp_1st_parent, second_parent.genes.begin() + fcp_2nd_parent, second_parent.genes.begin() + scp_2nd_parent);

	// 7. Delete items labeled as "to be deleted" from two previously created lists
	deleted_items.erase(std::remove_if(deleted_items.begin(), deleted_items.end(), IsItemToBeRemoved), deleted_items.end());
	inserted_items.erase(std::remove_if(inserted_items.begin(), inserted_items.end(), IsItemToBeRemoved), inserted_items.end());
	
	// 8. Search for duplicates
	// in the left part of the child solution
	for (int i = 0; i < inserted_items.size(); i++){
		for (int j = 0; j < fcp_1st_parent; j++){
			for (int k = 0; k < child.genes[j].items.size(); k++){
				if (inserted_items[i].GetId() == child.genes[j].items[k].GetId()){
					child.genes[j].items[k].SetId(-1);
					child.genes[j].to_be_removed = true;
				}
			}
		}
	}

	// in the right part of the child solution
	for (int i = 0; i < inserted_items.size(); i++){
		for (int j = fcp_1st_parent + (scp_2nd_parent - fcp_2nd_parent); j < child.genes.size(); j++){
			for (int k = 0; k < child.genes[j].items.size(); k++){
				if (inserted_items[i].GetId() == child.genes[j].items[k].GetId()){
					child.genes[j].items[k].SetId(-1);
					child.genes[j].to_be_removed = true;
				}
			}
		}
	}

	// 9. Remove duplicates:
	// From the left part of the child solution
	for (int i = 0; i < fcp_1st_parent; i++)
		child.genes[i].items.erase(std::remove_if(child.genes[i].items.begin(), child.genes[i].items.end(), IsItemToBeRemoved), child.genes[i].items.end());

	// From the right part of the child solution
	for (int i = fcp_1st_parent + (scp_2nd_parent - fcp_2nd_parent); i < child.genes.size(); i++)
		child.genes[i].items.erase(std::remove_if(child.genes[i].items.begin(), child.genes[i].items.end(), IsItemToBeRemoved), child.genes[i].items.end());

	// 10. Remove empty bins
	child.genes.erase(std::remove_if(child.genes.begin(), child.genes.end(), IsGeneEmpty), child.genes.end());

	// 11. Append items from "to be removed" genes to the list of deleted items
	for (int i = 0; i < child.genes.size(); i++){
		if (child.genes[i].to_be_removed)
			for (int j = 0; j < child.genes[i].items.size(); j++)
				deleted_items.push_back(child.genes[i].items[j]);
	}

	// 12. Remove bins labeled "to be removed"
	child.genes.erase(std::remove_if(child.genes.begin(), child.genes.end(), IsGeneToBeRemoved), child.genes.end());

	// 13. Apply Falkenauer's local search procedure to optimize the first child solution
    EFLocalSearchProcedure(deleted_items, child);

	// 14. If there are items on "deleted_items" list, apply FF heuristic to insert them into the first child
	if(!deleted_items.empty())
        FFHeuristic(deleted_items, child);
	
	deleted_items.clear();
	inserted_items.clear();
}

void BaseHGA::TwoPointCrossoverOpr(const Chromosome &first_parent, const Chromosome &second_parent, Chromosome &first_child, Chromosome &second_child){
	TwoPointCrossover(first_parent, second_parent, first_child);
	TwoPointCrossover(second_parent, first_parent, second_child);
}

// Mutation Operator
void BaseHGA::MutationOpr(Chromosome &chromosome, float fitness){
	vector<Item> deleted_items;
	
	for (int i = 0; i < chromosome.genes.size(); i++){
		if (chromosome.ComputeFitness(i) < fitness){
			for (int j = 0; j < chromosome.genes[i].items.size(); j++)
				deleted_items.push_back(chromosome.genes[i].items[j]);
			
			chromosome.DeleteGene(i);
		}
	}

	BFDHeuristic(deleted_items, chromosome);
}

// Checks whether chromosome if feasible or not
// 1. Contains all items
// 2. Does or does not have "duplicates")
bool BaseHGA::IsValid(const vector<Item> &items, const Chromosome& chromosome){
	//Check if "chromosome" contains all items
	bool found = false;
	for (int i = 0; i < items.size(); i++){
		found = false;
		for (int j = 0; j < chromosome.genes.size(); j++){
			for (int k = 0; k < chromosome.genes[j].items.size(); k++){
				if (chromosome.genes[j].items[k].GetId() == items[i].GetId()){
					found = true;
					break;
				}
			}
			if (found)
				break;
		}
		if (!found)
			return false;
	}

	// Does or does not have duplicates
	int duplicate = -1;
	for (int i = 0; i < items.size(); i++){
		duplicate = -1;
		for (int j = 0; j < chromosome.genes.size(); j++){
			for (int k = 0; k < chromosome.genes[j].items.size(); k++){
				if (chromosome.genes[j].items[k].GetId() == items[i].GetId())
					duplicate++;
			}
			if (duplicate > 0)
				return false;
		}
	}
}

void HGA::Core(Population &population, int nb_generations){
}
