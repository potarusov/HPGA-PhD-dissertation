#include <iostream>
#include "item.h"

Item::Item(void){
    m_id = 0;
    m_value = 0;
}

Item::Item(int id, int value){
    m_id = id;
    m_value = value;
}

Item::~Item(){
}

bool Item::operator < (const Item& item) const
{
	return (m_value < item.GetValue());
}

void Item::SetId(int id){
    m_id = id;
}

int Item::GetId() const{
    return m_id;
}

void Item::SetValue(int value){
    m_value = value;
}

int Item::GetValue() const{
    return m_value;
}

void Item::PrintItem(){
    cout << "Item ID: " << m_id << endl;
    cout << "Item Value: " << m_value << endl;
}


