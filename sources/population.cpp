#include <iostream>
#include "population.h"

Population::Population(void){
}

Population::~Population(){
    chromosomes.clear();
}

int Population::GetSize(){
    return chromosomes.size();
}

void Population::AppendChromosome(Chromosome &chromosome){
    chromosomes.push_back(chromosome);
}

void Population::DeleteChromosome(int index){
    if(index >= chromosomes.size() - 1)
        index = chromosomes.size() - 1;
    chromosomes.erase(chromosomes.begin() + index);
}

void Population::DeleteRangeOfChromosomes(int first_index, int last_index){
    if(last_index >= chromosomes.size() - 1)
        last_index = chromosomes.size() - 1;
    chromosomes.erase(chromosomes.begin() + first_index, chromosomes.begin() + last_index);
}

int Population::GetBestChromosome(){
    float best_fitness = chromosomes[0].ComputeFitness();
    int best_index = 0;

    for(int i = 1; i < chromosomes.size(); i++){
        float fitness = chromosomes[i].ComputeFitness();
        if(best_fitness < fitness){
            best_fitness = fitness;
            best_index = i;
        }
    }
    return best_index;
}

void Population::PrintBestChromosome(){
    int index_best_chromosome = GetBestChromosome();
    chromosomes[index_best_chromosome].PrintChromosome();
}

void Population::PrintPopulation(){
    cout << "Population Size: " << chromosomes.size() << endl;
    for(int i = 0; i < chromosomes.size(); i++){
        cout << "Chromosome: " << i << endl;
        chromosomes[i].PrintChromosome();
    }
}


