#include <iostream>
#include <vector>
#include "instanceloader.h"
#include "hga.h"

using namespace std;

int main(int argc, char *argv[])
{
	vector<Item> items;
	// Test data loading from a text file
	/*InstanceLoader loader("N1C1W1_A.BPP");
	loader.LoadData();
	
	for (int i = 0; i < loader.items.size(); i++){
		Item item(i, loader.items[i]);
		items.push_back(item);
	}*/
	
	// Items:
	// Bin 1: 37, 37, 26
	// Bin 2: 27, 55, 18
	// Bin 3: 14, 14, 14, 14, 14, 14, 16
	// Bin 4: 13, 23, 33, 19, 12
	// Bin 5: 5, 15, 25, 35, 2, 2, 16
	
	Item item0(0, 37);
	Item item1(1, 37);
	Item item2(2, 26);
	Item item3(3, 27);
	Item item4(4, 55);
	Item item5(5, 18);
	Item item6(6, 14);
	Item item7(7, 14);
	Item item8(8, 14);
	Item item9(9, 14);
	Item item10(10, 14);
	Item item11(11, 14);
	Item item12(12, 16);
	Item item13(13, 13);
	Item item14(14, 23);
	Item item15(15, 33);
	Item item16(16, 19);
	Item item17(17, 12);
	Item item18(18, 15);
	Item item19(19, 5);
	Item item20(20, 25);
	Item item21(21, 35);
	Item item22(22, 2);
	Item item23(23, 2);
	Item item24(24, 16);

	items.push_back(item0);
	items.push_back(item1);
	items.push_back(item2);
	items.push_back(item3);
	items.push_back(item4);
	items.push_back(item5);
	items.push_back(item6);
	items.push_back(item7);
	items.push_back(item8);
	items.push_back(item9);
	items.push_back(item10);
	items.push_back(item11);
	items.push_back(item12);
	items.push_back(item13);
	items.push_back(item14);
	items.push_back(item15);
	items.push_back(item16);
	items.push_back(item17);
	items.push_back(item18);
	items.push_back(item19);
	items.push_back(item20);
	items.push_back(item21);
	items.push_back(item22);
	items.push_back(item23);
	items.push_back(item24);
	
	// Chromosome1
	Chromosome ind1(100);
	Gene gene1, gene2, gene3, gene4, gene5, gene6;
	// Bin1
	gene1.AppendItem(item0);
	gene1.AppendItem(item3);
	gene1.AppendItem(item6);

	// Bin2
	gene2.AppendItem(item13);
	gene2.AppendItem(item18);
	gene2.AppendItem(item1);

	// Bin3
	gene3.AppendItem(item19);
	gene3.AppendItem(item2);
	gene3.AppendItem(item5);
	gene3.AppendItem(item8);
	gene3.AppendItem(item15);

	// Bin4
	gene4.AppendItem(item4);
	gene4.AppendItem(item7);
	gene4.AppendItem(item14);

	// Bin5
	gene5.AppendItem(item20);
	gene5.AppendItem(item9);
	gene5.AppendItem(item16);
	gene5.AppendItem(item21);
	
	// Bin6
	gene6.AppendItem(item10);
	gene6.AppendItem(item17);
	gene6.AppendItem(item22);
	gene6.AppendItem(item11);
	gene6.AppendItem(item23);
	gene6.AppendItem(item12);
	gene6.AppendItem(item24);

	//Duplicate
	//gene6.AppendItem(item23);

	ind1.AppendGene(gene1);
	ind1.AppendGene(gene2);
	ind1.AppendGene(gene3);
	ind1.AppendGene(gene4);
	ind1.AppendGene(gene5);
	ind1.AppendGene(gene6);

	// Chromosome2
	Chromosome ind2(100);
	Gene gene21, gene22, gene23, gene24, gene25, gene26, gene27, gene28, gene29;
	// Bin1
	gene21.AppendItem(item0);
	gene21.AppendItem(item3);
	gene21.AppendItem(item6);

	// Bin2
	gene22.AppendItem(item13);
	gene22.AppendItem(item18);
	gene22.AppendItem(item1);

	// Bin3
	gene23.AppendItem(item4);
	gene23.AppendItem(item7);
	gene23.AppendItem(item14);

	// Bin4
	gene24.AppendItem(item19);
	gene24.AppendItem(item2);
	gene24.AppendItem(item5);

	// Bin5
	gene25.AppendItem(item8);
	gene25.AppendItem(item15);
	gene25.AppendItem(item20);

	// Bin6
	gene26.AppendItem(item9);
	gene26.AppendItem(item16);
	gene26.AppendItem(item21);

	// Bin7
	gene27.AppendItem(item10);
	gene27.AppendItem(item17);
	gene27.AppendItem(item22);

	// Bin8
	gene28.AppendItem(item11);
	gene28.AppendItem(item23);
	gene28.AppendItem(item12);

	// Bin9
	gene29.AppendItem(item24);

	//Duplicate
	//gene29.AppendItem(item23);

	ind2.AppendGene(gene21);
	ind2.AppendGene(gene22);
	ind2.AppendGene(gene23);
	ind2.AppendGene(gene24);
	ind2.AppendGene(gene25);
	ind2.AppendGene(gene26);
	ind2.AppendGene(gene27);
	ind2.AppendGene(gene28);
	ind2.AppendGene(gene29);

	//Chromosome3
	Chromosome ind3(100);
	Gene gene31, gene32, gene33, gene34, gene35, gene36, gene37;
	// Bin1
	gene31.AppendItem(item0);
	gene31.AppendItem(item3);
	gene31.AppendItem(item6);

	// Bin2
	gene32.AppendItem(item13);
	gene32.AppendItem(item18);
	gene32.AppendItem(item2);
	gene32.AppendItem(item5);
	gene32.AppendItem(item8);

	// Bin3
	gene33.AppendItem(item15);
	gene33.AppendItem(item20);
	gene33.AppendItem(item10);
	gene33.AppendItem(item17);

	// Bin4
	gene34.AppendItem(item22);
	gene34.AppendItem(item12);
	gene34.AppendItem(item24);
	gene34.AppendItem(item1);

	// Bin5
	gene35.AppendItem(item4);
	gene35.AppendItem(item7);
	gene35.AppendItem(item14);

	// Bin6
	gene36.AppendItem(item19);
	gene36.AppendItem(item9);
	gene36.AppendItem(item16);
	gene36.AppendItem(item21);

	// Bin7
	gene37.AppendItem(item11);
	gene37.AppendItem(item23);

	//Duplicate
	//gene37.AppendItem(item23);

	ind3.AppendGene(gene31);
	ind3.AppendGene(gene32);
	ind3.AppendGene(gene33);
	ind3.AppendGene(gene34);
	ind3.AppendGene(gene35);
	ind3.AppendGene(gene36);
	ind3.AppendGene(gene37);
	
	Population population;
	population.AppendChromosome(ind1);
	population.AppendChromosome(ind2);
	population.AppendChromosome(ind3);
	
	HGA hga(items, population, 100, 0.5, 0.5);
	int first_parent = 0; //hga.TournamentSelectionOpr(population, 3);
	int second_parent = 1; //hga.RouletteWheelSelectionOpr(population);

	Chromosome child1(ind1), child2(ind2);

	// Test OnePointCrossover
	hga.OnePointCrossoverOpr(ind1, ind2, child1, child2);
	bool is_valid_child1 = hga.IsValid(items, child1);
	bool is_valid_child2 = hga.IsValid(items, child2);

	// Test TwoPointCrossover
	//hga.TwoPointCrossoverOpr(ind1, ind2, child1, child2);
	//bool is_valid_child1 = hga.IsValid(items, child1);
	//bool is_valid_child2 = hga.IsValid(items, child2);
	
	// Test Mutation
	//hga.MutationOpr(ind1, 0.7);
	//bool is_valid_ind1 = hga.IsValid(items, ind1);

    return 0;
}
