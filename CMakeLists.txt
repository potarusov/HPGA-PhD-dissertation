project(HPGA-PhD-dissertation)
cmake_minimum_required(VERSION 2.8)
include_directories ("${PROJECT_SOURCE_DIR}/includes")

add_executable( hpga
    sources/main.cpp
    sources/instanceloader.cpp
    sources/hga.cpp
    sources/population.cpp
    sources/chromosome.cpp
    sources/gene.cpp
    sources/item.cpp
    includes/instanceloader.h
    includes/hga.h
    includes/population.h
    includes/chromosome.h
    includes/gene.h
    includes/item.h)
