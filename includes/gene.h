#include <vector>
#include "item.h"
using namespace std;

// Gene
class Gene
{
public:
    vector<Item> items;
    int sum_of_items;
	bool to_be_removed;
    Gene(void);
    ~Gene();
	int GetSumOfItems();
    void AppendItem(Item &item);
    void PrintGene();
};
