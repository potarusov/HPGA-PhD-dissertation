#include <iostream>
#include <vector>
using namespace std;

// Instance Loader
class InstanceLoader
{
    const char *m_strDataFileName;
public:
    // Number of items
    int m_nb_items;

    // Bin capacity
    int m_bin_capacity;

    // Array of items
    vector<int> items;

    InstanceLoader(void);
    InstanceLoader(const char *strDataFileName);
    ~InstanceLoader();
    int LoadData();
    void PrintInstance();
};
