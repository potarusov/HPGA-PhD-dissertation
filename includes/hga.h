#include <vector>
#include "population.h"

// BaseHGA : base class for hybrid genetic algorithms
class BaseHGA
{
public:
    int m_bin_capacity;
    float m_crossover_probability;
    float m_mutation_probability;
    
    BaseHGA();
	BaseHGA(int bin_capacity, float crossover_probability, float mutation_probability);
    ~BaseHGA();

    // Heuristics
	// First Fit
	void FFHeuristic(vector<Item> &items_to_insert, Chromosome &chromosome);

    // Best Fit Decreasing
    void BFDHeuristic(vector<Item> &items_to_insert, Chromosome &chromosome);

	// Local search procedure proposed by E. Falkenauer
    void EFLocalSearchProcedure(vector<Item> &items_to_insert, Chromosome &chromosome);

    // Genetic Operators
    // Roulette Wheel Selection Operator
	int RouletteWheelSelectionOpr(Population &population);

    // Tournament Selection Operator
	int TournamentSelectionOpr(Population &population, int size_tournament);

	// One-Point Crossover: creates one child per call
	void OnePointCrossover(const Chromosome &first_parent, const Chromosome &second_parent, Chromosome &child);

	// One-Point Crossover: creates two children per call
	void OnePointCrossoverOpr(const Chromosome &first_parent, const Chromosome &second_parent, Chromosome &first_child, Chromosome &second_child);

	// Two-Point Crossover: creates one child per call
	void TwoPointCrossover(const Chromosome &first_parent, const Chromosome &second_parent, Chromosome &child);

    // Two-Point Crossover Operator: creates two children per call
    void TwoPointCrossoverOpr(const Chromosome &first_parent, const Chromosome &second_parent, Chromosome &first_child, Chromosome &second_child);

   	// Mutation Operator
    void MutationOpr(Chromosome& chromosome, float fitness);

	// Checks whether chromosome if feasible or not
	// 1. Contains all items
	// 2. Does or does not have "duplicates")
	bool IsValid(const vector<Item> &items, const Chromosome& chromosome);

    // Algorithm's body
    virtual void Core(Population &population, int nb_generations) = 0;
};

// HGA
class HGA : public BaseHGA
{
public:
	HGA(void) : BaseHGA(){
	}

	HGA(const vector<Item> &items, Population &population, int bin_capacity, float crossover_probability, float mutation_probability) :
		BaseHGA(bin_capacity, crossover_probability, mutation_probability){

	}
	~HGA(){
	}
	void Core(Population &population, int nb_generations);
};
