#include <vector>
#include "chromosome.h"
using namespace std;

// Population
class Population
{
public:
    vector<Chromosome> chromosomes;
    Population(void);
    ~Population();
    int GetSize();
    void AppendChromosome(Chromosome &chromosome);
    void DeleteChromosome(int index);
    void DeleteRangeOfChromosomes(int first_index, int last_index);
    int GetBestChromosome();
    void PrintBestChromosome();
    void PrintPopulation();
};
