#include <vector>
using namespace std;

// Item
class Item
{
    int m_id;
    int m_value;
public:
    Item(void);
    Item(int _id, int _value);
    ~Item();
	bool operator < (const Item& str) const;
    void SetId(int id);
    int GetId() const;
    void SetValue(int value);
    int GetValue() const;
    void PrintItem();
};
