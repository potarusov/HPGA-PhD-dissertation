#include <vector>
#include "gene.h"
using namespace std;

// Chromosome
class Chromosome
{
public:
    vector<Gene> genes;
    int m_bin_capacity;
    float m_fitness;
    Chromosome(void);
    Chromosome(int _capacity);
	Chromosome(const Chromosome &obj);
    ~Chromosome();
	
    float ComputeFitness();
    float ComputeFitness(int gene);
    float ComputeFitness(int start_gene, int end_gene);
    void InsertItemToGene(int gene_index_in_chromosome, Item &item);
    void InsertGeneToChromosome(int position, Gene &gene);
    void InsertArrayToChromosome(int position, std::vector<Gene> &arr);
    void AppendGene(Gene &gene);
	void DeleteGene(int gene);
    void PrintChromosome();
};
